package pg.eti.kssr.noisemonitor;

/**
 * Created by kacpe on 08.10.2017.
 */

public final class ParamsGlobal {
    public final static String prefName = "MyPref";
    public final static String phoneNumberParam = "phoneNumber";
    public final static String AudioFileName = "/audiorecordtest.3gp";
    public final static long recordTime = 5000;
    public final static long pauseTime = 5000;
    public final static long sampleTime = 100;
    ///////////////////////// tymaczowe rozwiazanie do testow
    public static boolean toDial = false;
}
