package pg.eti.kssr.noisemonitor;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SaveNumberActivity extends AppCompatActivity {

    private Button saveNumberButton;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_number);
        prefs = getSharedPreferences(ParamsGlobal.prefName, MODE_PRIVATE);
        EditText e = (EditText)findViewById(R.id.numberTextField);
        e.setText(prefs.getString(ParamsGlobal.phoneNumberParam,""));
        saveNumberButton = (Button)findViewById(R.id.save_numer_button);
        saveNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText e = (EditText)findViewById(R.id.numberTextField);
                String number = e.getText().toString();
                /*prawdopodobnie należało by zrobić oddzielną klasę obsługującą zapis/odczyt danych
                * ważne w przypadku dalszej rozbudowy i ewentualnej zmiany sposobu zapisu danych*/
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("phoneNumber",number);
                editor.commit();
                finish();
            }
        });

    }
}
