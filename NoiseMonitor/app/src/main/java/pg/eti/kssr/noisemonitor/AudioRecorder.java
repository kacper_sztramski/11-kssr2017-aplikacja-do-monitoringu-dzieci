package pg.eti.kssr.noisemonitor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;
import android.Manifest;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


/**
 * Created by kacpe on 19.10.2017.
 */

public class AudioRecorder  {

    private MediaPlayer mPlayer;
    private MediaRecorder mRecorder;
    private String mFileName;
    private ArrayList<Integer> Amplitudes;
    private ArrayList<Long> signalEnergy;
    private CountDownTimer recordingTimer;
    private CountDownTimer pauseTimer;
    private SharedPreferences prefs;

    public AudioRecorder(String mFile, SharedPreferences sp){
        mFileName = mFile;
        Amplitudes = new ArrayList<Integer>();
        signalEnergy = new ArrayList<Long>();
        prefs = sp;
        recordingTimer = new CountDownTimer(ParamsGlobal.recordTime, ParamsGlobal.sampleTime) {
            @Override
            public void onTick(long millisUntilFinished) {
                Amplitudes.add(mRecorder.getMaxAmplitude());
            }

            @Override
            public void onFinish() {
                System.out.println("nagranie - stop");
                stopRecording();
            }
        };
        pauseTimer = new CountDownTimer(ParamsGlobal.pauseTime, ParamsGlobal.sampleTime) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                System.out.println("pauza  - stop");
                startRecording();
            }
        };
    }

    public  void startRecording(){
        if (mRecorder==null)
            mRecorder = new MediaRecorder();
        System.out.println("nagranie - start");
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);


        try {
            mRecorder.prepare();
        } catch (IOException e) {
            //  Log.e(ASD,"prepare() failed");

            Log.e(e.getMessage(),e.getMessage());
        }

        mRecorder.start();
        Amplitudes.clear();
        recordingTimer.start();

    }




    public void stopRecording(){
        mRecorder.stop();
      //  mRecorder.release();
      //  mRecorder = null;
        CalculateAmpli();
        if (ParamsGlobal.toDial)
           return;
        pauseTimer.start();
        System.out.println("pauza - start");
    }

    private void CalculateAmpli() {
        long energy = 0;
        for (int i=0;i<signalEnergy.size();i++){
            System.out.println("indeks: "+i+" war:"+signalEnergy.get(i));
        }
        for ( Integer i: Amplitudes) {
            energy += i*i;
        }
        signalEnergy.add(energy);
        System.out.println("energia: "+energy+"--------------------------------------------");
        analyzeEnergy();
    }

    private void analyzeEnergy() {
        ///tutuaj będzie skomplikowana funcja która analizuje
        if (signalEnergy.size()<3)
            return;
        long lastSignal = signalEnergy.get(signalEnergy.size()-1);
        long prev1 = signalEnergy.get(signalEnergy.size()-2);
        long prev2 = signalEnergy.get(signalEnergy.size()-3);
        long prevS = (prev1 + prev2) / 2;
        if ((lastSignal - prevS)>50000){
           ParamsGlobal.toDial = true;
            signalEnergy.clear();
        }


    }

    public void startPlaying(){
        if (mPlayer==null)
            mPlayer = new MediaPlayer();
            try {
                mPlayer.setDataSource(mFileName);
                mPlayer.prepare();
                mPlayer.start();

            } catch (IOException e) {

            }


    }

    public List<Integer> getAmplitudes(){
        return Amplitudes;
    }

    public void stopPlaying(){
        mPlayer.stop();
        mPlayer = null;

    }

}
