package pg.eti.kssr.noisemonitor;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.net.Uri;

import java.io.IOException;
import java.util.List;

public class MainSettingsActivity extends AppCompatActivity {

    private Button settingsButton;
    private Button startWorkingButton;
    ///testowo
    private Button playButtonC;
    private Button recButtonC;
    private CountDownTimer checkLebelTimer;
    ///testowo
    private IncomingCallsReceiver inCall;
    private SharedPreferences prefs;
    private boolean recording = false, playback = false;
    private AudioRecorder audioRecorder = null;
    private String mFileName;
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_settings);
        prefs = getSharedPreferences(ParamsGlobal.prefName, MODE_PRIVATE);
        playButtonC = (Button)findViewById(R.id.playButton);
        recButtonC = (Button)findViewById(R.id.recButton);
        recButtonC.setBackgroundColor(Color.rgb(192,192,192));
        playButtonC.setBackgroundColor(Color.rgb(192,192,192));
        settingsButton = (Button) findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartSaveNumberActivity();
            }
        });
        startWorkingButton = (Button) findViewById(R.id.startListeningButton);
        startWorkingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartListening();
            }
        });
        recButtonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecording();
            }
        });

        playButtonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onPlayback();
            }
        });
        inCall = new IncomingCallsReceiver();
        inCall.onReceive(this.getBaseContext(), this.getIntent());
        //////////////////////////////testowo
        mFileName = getExternalCacheDir().getAbsolutePath();
        mFileName += ParamsGlobal.AudioFileName;
        checkLebelTimer = new CountDownTimer(200000, 2000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (ParamsGlobal.toDial)
                    MakePhoneCall();
            }

            @Override
            public void onFinish() {
                checkLebelTimer.start();
            }
        };
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
        //////////////////////////////
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) finish();
    }

    private void StartListening() {
        if (audioRecorder==null)
            audioRecorder = new AudioRecorder(mFileName,prefs);
        recButtonC.setBackgroundColor(Color.rgb(178,34,34));
        recording = true;
        audioRecorder.startRecording();
        checkLebelTimer.start();


 //       MakePhoneCall();
    }

    private void StartSaveNumberActivity() {

        Intent intent = new Intent(this, SaveNumberActivity.class);
        startActivity(intent);
    }

    private void MakePhoneCall() {
        String phoneNumber = prefs.getString(ParamsGlobal.phoneNumberParam, "");
        Intent i = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+48" + phoneNumber));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        ParamsGlobal.toDial = false;
        startActivity(i);
    }

    private void onRecording(){
  /*      if (audioRecorder==null)
            audioRecorder = new AudioRecorder(mFileName);
        if (recording){
            audioRecorder.stopRecording();
            recButtonC.setBackgroundColor(Color.rgb(192,192,192));
            recording = false;
        }
        else{
            audioRecorder.startRecording();
            recButtonC.setBackgroundColor(Color.rgb(178,34,34));
            recording = true;
        }*/
    }

    private void onPlayback(){
/*        if (audioRecorder==null)
            audioRecorder = new AudioRecorder(mFileName);
        if (playback){
            audioRecorder.stopPlaying();
            playButtonC.setBackgroundColor(Color.rgb(192,192,192));
            playback = false;
        }
        else {
            audioRecorder.startPlaying();
            playButtonC.setBackgroundColor(Color.rgb(34,139,34));
            playback = true;
        }*/
    }

}
